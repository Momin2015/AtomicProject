<?php
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."AtomicProject".DIRECTORY_SEPARATOR."vendor/autoload.php");

use Atomic\BITM\SEIP107921\Terms\terms;
use Atomic\BITM\SEIP107921\Message\Message;
use Atomic\BITM\SEIP107921\Utility\Utility;

$terms=new Terms();
$var = $terms->index();

?>




<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Terms & Condition</title>
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap-theme.css">
    </head>
    <body>
        <div class="container">
            <h1>Terms & Condition</h1>
            <a class="btn btn-primary" href="create.php">Create New</a>
            <a class="btn btn-primary" href="http://localhost/AtomicProject/index.php">Back</a>

                            <table class="table table-bordered">
                               <tr class="success"><th width="50">Serial</th><th>Name</th><th>Condition</th><th>Action</th></tr>
                               <?php foreach($var as $terms): ?>
                               <tr>
                                   <td><?php echo $terms['id'];?></td><td><?php echo $terms['name'];?></td><td><?php echo$terms['terms'];?></td>
                                     <td width="390">
                                         <div class="btn-group">
                                            <a class="btn btn-primary" href="view.php">View</a>
                                            <a class="btn btn-success" href="edit.php">Edit</a>
                                            <a class="btn btn-danger" href="delete.php">Delete</a>
                                            <a class="btn btn-primary" href="trash.php">Trash</a>
                                            <a class="btn">Recover</a>
                                            <a class="btn">Email To Friend</a>
                                         </div>
                                     </td>
                               </tr>
                              <?php endforeach; ?>




                            </table>
                    </div>
                </div>
            </div>
            
        </div>
                
                
               
            </center>
        </div>
    </body>
</html>
