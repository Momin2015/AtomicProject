<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Profile Picture</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap-theme.css">
        
  
</head>
        
        
    </head>
    <body>
         <a class="btn btn-primary" href="index.php">Back</a>
        <div class="container">
          
                
            <div id="loginbox" style="margin-top:20px;" class="mainbox col-md-8 col-md-offset-0 col-sm-8 col-sm-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h1>Profile Picture Upload</h1>
                        
                    </div>
                    <div class="panel-body">
                        <form role="form" action="store.php" method="post" autocomplete="off" enctype="multipart/form-data">
                            <div class="form-group">
                                <div class="input-group">
                                        <span class="input-group-addon">Your Name</span>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="Enter Your Name" required >
                                </div>
                            </div>   
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Select Image</span>
                                    <input type="file" name="image" class="form-control" id="image" reqiured>
                                </div>
                            </div>
                            <button type="reset" name="submit" class="btn btn-info">Reset</button>
                             <button type="submit" name="submit" class="btn btn-primary">Submit</button>

                          </form>  

                    </div>
                </div>
            </div>
                
                
        </div>
        <script src="../js/bootstrap-min.js"></script>
    </body>
</html>