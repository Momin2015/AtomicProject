<?php

include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."AtomicProject".DIRECTORY_SEPARATOR."vendor/autoload.php");

use Atomic\BITM\SEIP107921\Profile\profile;

$profile = new Profile();
$profiles=$profile->index();

?>




<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Profile Information</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap-theme.css">
    </head>
    <body>
       
        

        <div class="container">
                            
                           
                                <h1>Profile Information</h1>
                                 <a class="btn btn-primary" href="create.php">Create New</a>
                                <a class="btn btn-primary" href="http://localhost/AtomicProject/index.php">Back</a>
                                
                            
                        
                        
                            <table class="table table-bordered">
                               <tr class="success"><th width="50">Serial</th><th>Name</th><th>Image</th><th>Action</th></tr>
                               <?php foreach($profiles as $profile): ?>
                               <tr><td><?php echo $profile['id'];?></td><td><?php echo $profile['name'];?></td>
                                   <td width="100"> <?php echo "<img class='img-rounded' alt='Cinque Terre' width='80' height='50' src='userphoto/".$profile['image']."'  />"; ?>
                                   </td>
                                     <td width="390">
                                         <div class="btn-group">
                                            <a class="btn btn-primary" href="view.php">View</a>
                                            <a class="btn btn-success" href="edit.php">Edit</a>
                                            <a class="btn btn-danger" href="delete.php">Delete</a>
                                            <a class="btn btn-primary" href="trash.php">Trash</a>
                                            <a class="btn">Recover</a>
                                            <a class="btn">Email To Friend</a>
                                         </div>
                                     </td>
                               </tr>
                              <?php endforeach; ?>




                            </table>
                    </div>
           
                
                
               
          
      
    </body>
</html>
