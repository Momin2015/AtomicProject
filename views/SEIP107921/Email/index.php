<?php
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."AtomicProject".DIRECTORY_SEPARATOR."vendor/autoload.php");
use Atomic\BITM\SEIP107921\Email\email;
$email= new Email();
$var=$email->index();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>EMAIL</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap-theme.css">
    </head>
    <body>
        <div class="container">
            <h1>Email List</h1>
            <a class="btn btn-primary" href="create.php">Create New</a>
             <a class="btn btn-primary" href="http://localhost/AtomicProject/index.php">Back</a>
            <table class="table table-bordered">
                <tr class="success"><th width="50px">Serial</th><th>Name</th><th>Email</th><th>Action</th></tr>
                <?php foreach($var as $email):?>
                <tr>
                    <td><?php echo $email[$id];?></td>
                    <td><?php echo $email[$name];?></td>
                    <td><?php echo $email[$email];?></td>
                    <td width="390px">
                        <div class="btn-group">
                            <a class="btn btn-success" href="view.php"></a>
                            <a class="btn btn-info" href="update.php"></a>
                            <a class="btn btn-primary" href="delete.php"></a>
                            <a class="btn btn-primary" href="trash.php">Trash</a>
                            <a class="btn">Recover</a>
                            <a class="btn">Email To Friend</a>
                        </div>
                    </td>
                </tr>
                <?php endforeach;?>

            </table>
        </div>
    </body>
</html>


