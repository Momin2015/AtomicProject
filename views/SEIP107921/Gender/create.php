<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Gender Entry</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap-theme.css">
        
        
    </head>
    <body>
         <a class="btn btn-primary" href="index.php">Back</a>
         <div class="container">

                       <h1>Gender Entry</h1>
          


                   <form role="form" method="post" action="store.php" >
                            <div class="form-group">
                              <label for="usr">Enter Name :</label>
                              <input type="text" name="name" class="form-control" id="usr" value="" placeholder="enter your name here"  required/>
                            </div>
                            <label for="gender">Select Gender :</label>
                            <div class="radio-inline">
                              <label><input type="radio" name="gender" value="Male"   required >Male</label>
                            </div>
                            <div class="radio-inline">
                              <label><input type="radio" name="gender" value="Female"   required >Female</label>
                            </div><br>
                            <button type="reset" class="btn btn-primary">Reset</button>
                            <button type="submit" class="btn btn-success" name="submit">Submit</button>

                   </form>     

          
         </div>  
       
            
        
                
               
         <script src="../js/bootstrap-min.js"></script>
    </body>
</html>
