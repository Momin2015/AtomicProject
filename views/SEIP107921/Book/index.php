<?php
session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."AtomicProject".DIRECTORY_SEPARATOR."vendor/autoload.php");

use Atomic\BITM\SEIP107921\book\book;
use Atomic\BITM\SEIP107921\Message\Message;
use Atomic\BITM\SEIP107921\Utility\Utility;

$book = new Book();
$var =$book->index();

?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>BOOK TITLE</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap-theme.css">
    </head>
    <body>
        <div class="container">
            <h1>Book List</h1>
            <a class="btn btn-primary" href="create.php">Create New</a>
            <a class="btn btn-primary" href="http://localhost/AtomicProject/index.php">Back</a>

        
        <div><span>Search / Filter| </span> 
            <span id="utility">Download as PDF | XL</span>
            <select>
                <option>10</option>
                <option>20</option>
                <option>30</option>
                <option>40</option>
                <option>50</option>
            </select>
        </div>
            <table class="table table-bordered">
                <tr class="success"><th width="50px">Serial</th><th>Book Title</th><th>Author</th><th>Action</th></tr>
                <?php foreach($var as $book):?>
                <tr>
                    <td><?php echo $book['id']; ?></td>
                    <td><?php echo $book['title']; ?></td>
                    <td><?php echo $book['author']; ?></td>
                    <td width="390px">
                        <div class="btn-group">
                            <a class="btn btn-success" href="view.php">View</a>
                            <a class="btn btn-info" href="edit.php">Update</a>
                            <a class="btn btn-danger" href="delete.php">Delete</a>
                            <a class="btn btn-primary" href="trash.php">Trash</a>
                            <a class="btn">Recover</a>
                            <a class="btn">Email To Friend</a>
                           
                        </div>
                    </td>
                </tr> 
                <?php endforeach; ?>
            </table>
        </div>

    </body>
</html>

