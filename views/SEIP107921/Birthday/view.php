<?php

session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."AtomicProject".DIRECTORY_SEPARATOR."vendor/autoload.php");

use Atomic\BITM\SEIP107921\Birthday\Birthday;

$birthday = new Birthday();
$var =$birthday->view($_GET['id']);

?>


<html>
    <head>
        <title>Birthday Data</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap-theme.css">
    </head>
    <body>
        <div class="container">
            <a class="btn btn-primary" href="index.php">Back</a>
                        
                            <table class="table table-bordered">
                                 <tr class="success"><th width="50"><h4>ID</h4></th><th><h4>Name</h4></th><th></h4>birthday</h4></th><th><h4>Action</h4></th></tr>
                                 
                                 <tr><td><h4><?php echo $var->id ;?></h4></td><td><h4><?php echo $var->name;?></h4></td><td><h4><?php echo $var->birthday;?></h4></td>
                                       <td width="150">
                                        <a class="btn btn-info" href="edit.php?id=<?php echo $var->id;?>">Edit</a>
                                         <a class="btn btn-danger" href="delete.php?id=<?php echo $var->id;?>">Delete</a>
                                        
                                       </td>
                                 </tr>
                         </table>
                        </div>
                    
                             
       
        <script src="../js/bootstrap.js"></script>
        <script src="../jslib/dist/sweetalert.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function(){
                $('#delete').bind('click',function(e){
                  
                    var isOk = confirm("Are you sure you want to delete?");
                    //console.log(isOk);
                    if(!isOk){
                        e.preventDefault();
                    }
             });
                
            });
           
        </script>
    </body>
</html>