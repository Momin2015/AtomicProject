<?php
session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."AtomicProject".DIRECTORY_SEPARATOR."vendor/autoload.php");

use Atomic\BITM\SEIP107921\Birthday\Birthday;

$birthday = new Birthday();
$var =$birthday->edit($_GET['id']);

?>
<html>
    <head>
        <title>Edit an Item</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap-theme.css">
    </head>
    <body>
        <a class="btn btn-primary" href="index.php">Back</a>
        <div class="container">
        <h1>Edit an Item</h1>
        
        <form action="update.php" method="post" role="form" entype="multipart/from-data">

                <div class="form-group">
                    <label for="name"> Name</label>
                    <input 
                        name="name"
                        id="name"
                        autofocus="true"
                        tabindex="10"
                        placeholder="Please edit name"
                    />

                </div>
                
                <div class="form-group">
                    <label for="date"> Birthday</label>
                    <input 
                        name="birthday"
                        id="date"
                        autofocus="true"
                        tabindex="10"
                        placeholder="Please edit birthday"
                    />

                </div>

           
                <button type="reset" name="submit" class="btn btn-info">Reset</button>
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
<!--                <button  type="submit">Save & Create Again</button>
            -->
<!--            <input type="submit" value="Save" />
                <input type="button" value="Click Me!" />
                <input type="reset" value="Reset" />-->
    
        </form>
        

        </div>
    <script src="../js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
</html>
