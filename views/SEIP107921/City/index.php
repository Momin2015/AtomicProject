<?php
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."AtomicProject".DIRECTORY_SEPARATOR."vendor/autoload.php");
use Atomic\BITM\SEIP107921\City\city;

$city = new City();
$citys=$city->index();
?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>City List</title>
         <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap-theme.css">
    </head>
    <body>
          <div class="container">
       
                <h1>City List</h1>
                  <a class="btn btn-primary" href="create.php">Create New</a>
                  <a class="btn btn-primary" href="http://localhost/AtomicProject/index.php">Back</a>
  
             
                   
                 <table class="table table-bordered">
                     <tr class="success"><th width="50px">Serial</th><th>City</th><th>Action</th></tr>
                <?php foreach($citys as $city):?>

                   <tr>
                       <td><?php  echo $city['id'] ?></td>
                       <td><?php  echo $city['city'] ?></td>
                       <td width="390px">
                        <div class="btn-group">
                            <a class="btn btn-success" href="view.php">View</a>
                            <a class="btn btn-info" href="edit.php">Update</a>
                            <a class="btn btn-danger" href="delete.php">Delete</a>
                            <a class="btn btn-primary" href="trash.php">Trash</a>
                            <a class="btn">Recover</a>
                            <a class="btn">Email To Friend</a>
                        </div>
                    </td>
                      
                   </tr>
                   <?php endforeach; ?>
                   
                
                
                
                </table>
            
        </div>
                
                
               
          
        </div>
    </body>
</html>
