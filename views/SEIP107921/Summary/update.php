<?php
session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."AtomicProject".DIRECTORY_SEPARATOR."vendor/autoload.php");

use Atomic\BITM\SEIP107921\Summary\summary;
use Atomic\BITM\SEIP107921\Message\Message;
use Atomic\BITM\SEIP107921\Utility\Utility;

$summary = new Summary();
$summary->prepare($_REQUEST)->update();
?>
