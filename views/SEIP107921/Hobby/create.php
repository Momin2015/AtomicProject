<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Hobby</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap-theme.css">
        
  
</head>
        
        
    </head>
    <body>
          <a class="btn btn-primary" href="index.php">Back</a>

        <div class="container">
          <h1>Hobby Entry</h1>

                 
                        <form role="form" action="store.php" method="post" autocomplete="off">
                            <div class="form-group">
                                <div class="input-group">
                                        <span class="input-group-addon">Enter Name</span>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name" required >
                                </div>
                            </div>   
                            <div class="form-group">
                                Select Your Hobby   :
                                    <label class="checkbox-inline"><input type="checkbox" name="hobby[]" value="Gardening">Gardening</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="hobby[]" value="Stamp Collection">Stamp Collection</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="hobby[]" value="Traveling">Traveling</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="hobby[]" value="Reading Books">Reading</label>
                                
                            </div>
                <button type="reset" name="submit" class="btn btn-info">Reset</button>
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                          </form>  

                    </div>

        <script src="../js/bootstrap-min.js"></script>
    </body>
</html>