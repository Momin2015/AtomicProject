<?php
session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."AtomicProject".DIRECTORY_SEPARATOR."vendor/autoload.php");

use Atomic\BITM\SEIP107921\Hobby\hobby;

$hobby = new Hobby();
$var =$hobby->edit($_GET['id']);

?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        
        <title>Hobby Entry</title>
         <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap-theme.css">
        
    </head>
    <body>
        <a class="btn btn-primary" href="index.php">Back</a>
        <div class="container">

                <h1>Update Hobby</h1>

            <form action="update.php" method="post" role="form" entype="multipart/from-data">
                <table>
                 <input type="hidden" name="id" value="<?php echo $var->id;?>">
                 <tr><td>Name :</td><td><input type="text" name="name" value="<?php echo $var->name;?>" placeholder="enter your name here"  required/></td></tr><br>
                <tr><td>Hobby :</td><td><input type="checkbox" name="hobby[]" value="gardenning" <?php $z=explode(", ",$var->hobby);if(in_array("Gardening",$z)){echo "checked";}?> >Gardenning
                        <input type="checkbox" name="hobby[]" value="Stamp Collection" <?php $z=explode(", ",$var->hobby);if(in_array("Stamp Collection",$z)){echo "checked";}?> >Stamp Collection</td></tr><br>
                 <tr><td></td><td><input class="btn btn-success" type="reset"  value="Reset" />
                <input class="btn btn-info" type="submit" name="Update" value="Update" /></td></tr>
                
                
                </table>
                
            </form>
            
       
            
            </div>
           
                
        <script src="../js/bootstrap.js"></script>
        <script src="../jslib/dist/sweetalert.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>           

    </body>
</html>
