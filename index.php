<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" media="all" />
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-theme.min.css" media="all" />
        <title>Atomic Project</title>

        <style>
            body{
                background-color: #f6f6f6;

            }
            .col-md-12{
                background-color:#E9573F;
                color:#fff;
                height:100px;
               
            }
            .col-md-6{
                background-color:#66afe9;
                color:#fff;
                height:450px;
             
            }
            .col-sm-12{
                background-color:#656D78;
                color:#fff;
                height:50px;
                margin-top:-15px;
                padding-top:-85px;
               
            }

        </style>
    </head>
    <body>
            <div class="col-md-12">
            <center><h1>Atomic Project</h1></center>
                <ul class="pager">
                    <li><a href="#">Name:Abdullah Al Momin</a></li>
                    <li><a href="#">ID:107921</a></li>
                </ul>
            </div>

            <div class="container">

            <div class="col-md-6">
                <table class="table-responsive table-bordered">
                   <tr class="danger"><td>Serial</td><td>Project Name</td><td>View</td></tr>
                   <tr class="primary"><td>01</td><td>Book Title</td><td><a type="button" class="btn btn-block btn-primary" href="views/seip107921/book">Enter</a></td></tr>
                   <tr class="success"><td>02</td><td>Birthday</td><td><a type="button" class="btn btn-block btn-success" href="views/seip107921/birthday">Enter</a></td></tr>
                   <tr class="info"><td>03</td><td>City</td><td><a type="button" class="btn btn-block btn-info" href="views/seip107921/city">Enter</a></td></tr>
                   <tr class="warning"><td>04</td><td>Email Subscription</td><td><a type="button" class="btn btn-block btn-warning" href="views/seip107921/email">Enter</a></td></tr>
                   <tr class="danger"><td>05</td><td>Profile Picture</td><td><a type="button" class="btn btn btn-danger" href="views/seip107921/profile">Enter</a></td></tr>
                   <tr class="primary"><td>06</td><td>Gender</td><td><a type="button" class="btn btn-block btn-primary" href="views/seip107921/gender">Enter</a></td></tr>
                   <tr class="success"><td>07</td><td>Summary Of Organizationr</td><td><a type="button" class="btn btn-block btn-success" href="views/seip107921/summary">Enter</a></td></tr>
                   <tr class="info"><td>08</td><td>Hobby</td><td><a type="button" class="btn btn-block btn-info" href="views/seip107921/hobby">Enter</a></td></tr>
                   <tr class="warning"><td>09</td><td>Terms & Condition</td><td><a type="button" class="btn btn-block btn-warning" href="views/SEIP107921/Terms">Enter</a></td></tr>               
                
                
                
                </table>
            </div>
            
            </div> 
            <div class="col-sm-12">
            <center><h3>&copy 2016 | Atomic Project</h3></center>
               
            </div>
        <script src="bootstrap/js/bootstrap-min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    </body>
</html>
